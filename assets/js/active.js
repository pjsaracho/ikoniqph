let mainNavLinks = document.querySelectorAll("nav ul li");
let mainSections = document.querySelectorAll("section");
let lastId;
let cur = [];

const windowScrollHandler = event => {
  let fromTop = window.scrollY + 200;

  mainNavLinks.forEach(li => {
    let link = li.children[0];
    let section = document.querySelector(link.hash).nextElementSibling;
    if (
      section.offsetTop - 100 <= fromTop &&
      section.offsetTop + section.offsetHeight - 100 > fromTop
    ) {
      li.classList.add("active");
    } else {
      li.classList.remove("active");
    }
  });
};
window.addEventListener("load", windowScrollHandler, false);
window.addEventListener("scroll", windowScrollHandler, false);

const windowResizeHandler = event => {
  let fromTop = window.scrollY + 100;

  const activeLink = document.querySelector('nav ul li.active a');
  const activeSection = document.querySelector(activeLink.hash);
  let contents = [...document.querySelectorAll('section.row .content')];
  contents = [...contents, ...document.querySelectorAll('.process > .content > .row > div')];
  contents.forEach( c => {
    if(c.parentElement.classList.contains('row-y-center'))
      c.parentElement.style.minHeight = c.offsetHeight+"px";
  });
  if(activeLink.hash !== "#home")
    activeSection.style.marginTop = "100px";
  if(activeLink.hash !== "#process")
    window.scrollTo(0, activeSection.offsetTop);
};
// window.addEventListener("load", windowResizeHandler, false);
// window.addEventListener("resize", windowResizeHandler, false);

let currentAbout = document.querySelector('#about-about');
const aboutBtns = document.querySelectorAll('.about-btn');
const missionBtns = document.querySelectorAll('.mission-btn');
const visionBtns = document.querySelectorAll('.vision-btn');

const numLinks = document.querySelectorAll(".faqs .num"); 
const faqs = document.querySelectorAll(`[id^="faq-"]`);
let currentFaq = faqs[0];
const faqsScrollUp = function(who) {
  // let scroll = (currentAbout.dataset.pos*1 < who.dataset.pos*1) ? 'scroll-up-hide' : 'scroll-down-hide';
  currentFaq.classList.remove('in-view')

  setTimeout(function() {
    currentFaq.style.display = "none";
    //currentAbout.classList.remove(scroll);
    who.style.display = "flex";  
    // who.classList.add('in-view');
    
    currentFaq = who;
  }, 700);

  setTimeout(function() {
    who.classList.add('in-view');
    
  }, 800);

};
numLinks.forEach(n => {
  n.addEventListener('click', ()=> {
    faqsScrollUp(faqs[n.textContent*1]);
  })
})

document.querySelectorAll('.faqsBtn').forEach(btn => {
  btn.addEventListener('click', () => {
    faqsScrollUp(faqs[0]);
  });
})

const aboutScrollUp = function(who) {
  // let scroll = (currentAbout.dataset.pos*1 < who.dataset.pos*1) ? 'scroll-up-hide' : 'scroll-down-hide';
  // currentAbout.classList.add(scroll);
  currentAbout.classList.remove('in-view')

  setTimeout(function() {
    currentAbout.style.display = "none";
    //currentAbout.classList.remove(scroll);
    who.style.display = "block";  
    // who.classList.add('in-view');
    
    currentAbout = who;
  }, 700);

  setTimeout(function() {
    who.classList.add('in-view');
    
  }, 800);

};

missionBtns.forEach( btn => {
  btn.addEventListener("click", function() {
    aboutScrollUp(document.querySelector('#about-mission'));
  });
})

visionBtns.forEach( btn => {
  btn.addEventListener("click", function() {
    aboutScrollUp(document.querySelector('#about-vision'));
  });
});
aboutBtns.forEach( btn => {
  btn.addEventListener("click", function() {
    aboutScrollUp(document.querySelector('#about-about'));
  });
});

window.onload = () => {
  $('.carousel').carousel('pause')
}

const comm1 = document.querySelector('#comm-1');
const comm2 = document.querySelector('#comm-2');

const projectLinks = document.querySelectorAll('.projects a');
projectLinks.forEach( l => {
  if(l.classList.contains('carousel-control-prev') || l.classList.contains('carousel-control-next')) {

  } else {
    const section = document.querySelector(l.hash);
    l.addEventListener('click', function(e) {
      e.preventDefault();
      window.scrollTo(0, section.getBoundingClientRect().top+window.scrollY-100);
    })
  }
})

// comm1Btn.addEventListener('click', function(e) {
//   e.preventDefault();
//   window.scrollTo(0, comm2.getBoundingClientRect().top+window.scrollY-100);
// })

// comm2Btn.addEventListener('click', function(e) {
//   e.preventDefault();
//   window.scrollTo(0, comm1.getBoundingClientRect().top+window.scrollY-100);
// })